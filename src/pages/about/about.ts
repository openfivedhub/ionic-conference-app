import { Component} from '@angular/core';
import { NavController, AlertController, Events } from 'ionic-angular';
@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  step: any;
  stepCondition: any;
  stepDefaultCondition: any;
  currentStep: any;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public evts: Events) {
    this.step = 1;
    this.stepCondition = true;
    this.stepDefaultCondition = this.stepCondition;
    this.evts.subscribe('step:changed', step => {
      this.currentStep = step;
      this.stepCondition = this.stepDefaultCondition;
    });
    this.evts.subscribe('step:next', () => {
      console.log('Next pressed: ', this.currentStep);
    });
    this.evts.subscribe('step:back', () => {
      console.log('Back pressed: ', this.currentStep);
    });
  }
  data = {};
  onFinish() {
  	console.log(this.data)
    this.alertCtrl.create({
      message: 'Finished!!',
      title: 'Work here',
      buttons: [{
        text: 'Ok'
      }]
    }).present();
  }

}
